<?php
    $language = array(
        'alreadyParticipated' => 'Tu as déjà participé.',
        'networkError' => 'Une erreur de communication est apparu. Réessayer plus tard, STP.',
        'email' => 'E-mail',
        'emailError' => 'Entre un e-mail valide, STP',
        'firstName' => 'Prénom',
        'firstNameError' => 'Entre ton prénom, STP',
        'formHead' => 'Inscription',
        'introHead' => 'Bonne chance!',
        'introText' => 'Réponds à la question suivante, STP; plusieurs réponses correctes possibles!',
        'lastName' => 'Nom',
        'lastNameError' => 'Entre ton nom, STP',
        'mtq1' => '
            CFC de<br>
            constructeur / trice<br>
            métallique
        ',
        'mtq1Head' => 'Un constructeur métallique &hellip;',
        'mtq1a' => 'réalise entre autres des vitrines, des avant-toits, des balcons, des escaliers et des balustrades',
        'mtq1b' => 'doit aussi pouvoir réparer des moteurs',
        'mtq1c' => 'travaille avec divers matériaux (parmi lesquels le verre)',
        'mtq1d' => 'n\'est habilité à souder qu\'après avoir terminé sa formation',
        'mtq1e' => 'travaille aussi avec des machines pilotées par ordinateur',
        'mtq2' => '
                CFC de<br>
                dessinateur / trice-<br>
                constructeur / trice<br>
                sur métal
        ',
        'mtq2Head' => 'Un dessinateur-constructeur sur métal &hellip;',
        'mtq2a' => 'est rarement amené à construire, dessiner et organiser',
        'mtq2b' => 'gère des projets de construction métallique, d\'acier, de fenêtres et de façades',
        'mtq2c' => 'n\'a pas besoin de posséder de bonnes capacités de représentation technique',
        'mtq2d' => 'devrait trouver son bonheur dans l\'algèbre et la géométrie',
        'mtq2e' => 'est bon en dessin technique',
        'mq1' => '
                CFC de<br>
                mécanicien(ne) en<br>
                machines agricoles
        ',
        'mq1Head' => 'Un mécanicien en machines agricoles &hellip;',
        'mq1a' => 'effectue surtout des changements d\'huile sur des tracteurs',
        'mq1b' => 'exécute aussi des réparations sur des récolteuses',
        'mq1c' => 'apprend le maniement des systèmes de diagnostic, des machines et des outils',
        'mq1d' => 'doit posséder une compréhension technique',
        'mq1e' => 'travaille le plus souvent à l\'air libre',
        'mq2' => '
                CFC de<br>
                mécanicien(ne) en<br>
                machines de chantier
        ',
        'mq2Head' => 'Un mécanicien en machines de chantier &hellip;',
        'mq2a' => 'n\'a aucun contact avec les clients pendant la formation',
        'mq2b' => 'ne travaille qu\'en atelier',
        'mq2c' => 'possède des connaissances en systèmes électriques et hydrauliques de machines de chantier',
        'mq2d' => 'peut réparer autant les tronçonneuses que les grosses pelles mécaniques',
        'mq2e' => 'travaille avec des installations de soudage, un pont-grue roulant et divers appareils de diagnostic (ordinateur portable, par exemple)',
        'mq3' => '
                CFC de<br>
                mécanicien(ne)<br>
                d\'appareils<br>
                à moteur
        ',
        'mq3Head' => 'Un mécanicien d\'appareils à moteur &hellip;',
        'mq3a' => 'peut aussi recourir à des connaissances informatiques dans sa formation',
        'mq3b' => 'n\'a besoin de vérifier, sur les appareils, aucune prescription relative à la sécurité, à la santé ni à l\'environnement',
        'mq3c' => 'apprend comment on répare les machines d\'entretien des pelouses, de service d\'hiver et de nettoyage',
        'mq3d' => 'ne mène jamais de discussion de vente directement avec un client',
        'mq3e' => 'travaille sur des appareils et machines motorisés',
        'noAnswers' => 'Nous n\'avons malheureusement reçu aucune réponse; essaye encore une fois, STP!',
        'pageTitle' => 'Union Suisse de Metal',
        'playWin' => 'Play &amp; Win',
        'startText' => '
                Participe au grand jeu gagnant de<br>
                l’Union Suisse du Métal et remporte un iPad!
        ',
        'startCaption' => '
                Clique ici<br>
                pour entrer dans<br>
                le jeu!
        ',
        'thankYouHead' => 'Un grand merci pour ta participation',
        'thankYouText' => '
                Tu recevras prochainement par e-mail d\'autres informations sur les professions offertes par l\'Union
                Suisse du Métal. Les gagnants de ce jeu de connaissances seront prévenus par e-mail.
        ',
        'thankYouFooter' => 'Aucune correspondance ne sera tenue sur ce concours.',
        'language' => 'fr'
    );