<?php
    $language = array(
        'alreadyParticipated' => 'Du hast bereits teilgenommen. Sorry!',
        'networkError' => 'Es ist ein Kommunikationsfehler aufgetreten. Bitte versuche es später erneut.',
        'email' => 'E-Mail',
        'emailError' => 'Bitte gib eine gültige E-Mail Adresse ein',
        'firstName' => 'Vorname',
        'firstNameError' => 'Bitte gib Deinen Vornamen ein',
        'formHead' => 'Anmeldung',
        'introHead' => 'Viel Glück!',
        'introText' => 'Bitte beantworte die nachfolgende Frage, es sind mehrere Antworten richtig!',
        'lastName' => 'Nachname',
        'lastNameError' => 'Bitte gib Deinen Nachnamen ein',
        'mtq1' => 'Metallbauer / in EFZ',
        'mtq1Head' => 'Ein Metallbauer &hellip;',
        'mtq1a' => 'erstellt unter anderem Schaufenster, Vordächer, Balkone, Treppen und Geländer',
        'mtq1b' => 'muss auch Motoren reparieren können',
        'mtq1c' => 'arbeitet mit verschiedenen Materialien (darunter auch Glas)',
        'mtq1d' => 'darf erst nach abgeschlossener Ausbildung schweissen',
        'mtq1e' => 'arbeitet auch mit computergesteuerten Maschinen',
        'mtq2' => '
            Metallbaukonst-<br>
            rukteur / in EFZ
        ',
        'mtq2Head' => 'Ein Metallbaukonstrukteur &hellip;',
        'mtq2a' => 'konstruiert, zeichnet und organisiert selten',
        'mtq2b' => 'bearbeitet Projekte im Metall-, Stahl-, Fenster- und Fassadenbau',
        'mtq2c' => 'sollte über kein gutes technisches Vorstellungsvermögen verfügen',
        'mtq2d' => 'sollte Freude an der Algebra und der Geometrie haben',
        'mtq2e' => 'ist gut im technischen Zeichnen',
        'mq1' => '
            Landmaschinen-<br>
            mechaniker / in EFZ
        ',
        'mq1Head' => 'Ein Landmaschinenmechaniker &hellip;',
        'mq1a' => 'macht vor allem Ölwechsel an Traktoren',
        'mq1b' => 'führt auch Reparaturen an Erntemaschinen durch',
        'mq1c' => 'lernt den Umgang mit Diagnosesystemen, Maschinen und Werkzeugen',
        'mq1d' => 'braucht technisches Verständnis',
        'mq1e' => 'arbeitet meistens im Freien',
        'mq2' => '
            Baumaschinen-<br>
            mechaniker / in EFZ
        ',
        'mq2Head' => 'Ein Baumaschinenmechaniker &hellip;',
        'mq2a' => 'hat während der Ausbildung keinen Kundenkontakt',
        'mq2b' => 'arbeitet nur in der Werkstatt',
        'mq2c' => 'kennt sich aus mit elektrischen und hydraulischen Systemen von Baumaschinen',
        'mq2d' => 'kann sowohl Motorsägen als auch grosse Bagger reparieren',
        'mq2e' => 'arbeitet mit Schweissanlagen, Hallenkran und verschiedenen Diagnosegeräten wie z.B Laptop',
        'mq3' => '
            Motorgeräte-<br>
            mechaniker / in EFZ
        ',
        'mq3Head' => 'Ein Motorgerätemechaniker &hellip;',
        'mq3a' => 'kann auch Computer-Kenntnisse in seiner Ausbildung einsetzen',
        'mq3b' => 'braucht keine Sicherheits-, Gesundheits- und Umweltvorschriften an Geräten zu überprüfen',
        'mq3c' => 'lernt, wie man Rasenpflegegeräte, Winterdienst- und Reinigungsmaschinen repariert',
        'mq3d' => 'führt nie direkt mit einem Kunden Verkaufsgespräche',
        'mq3e' => 'arbeitet an motorisierten Geräten und Maschinen',
        'noAnswers' => 'Leider haben wir keine Antwort erhalten, bitte versuche es nochmals!',
        'pageTitle' => 'Schweizer Metall-Union',
        'playWin' => 'Play &amp; Win',
        'startText' => '
            Nimm teil am grossen Gewinnspiel der<br>
            Schweizerischen Metall-Union und gewinne einen iPad!
        ',
        'startCaption' => '
            Hier klicken<br>
            und schon bist<br>
            du dabei.
        ',
        'thankYouHead' => 'Vielen Dank für Deine Teilnahme',
        'thankYouText' => '
            Du wirst demnächst per E-Mail weitere Informationen über die Berufe bei der Schweizerischen
            Metall-Union erhalten. Gewinner des Wissenspiels werden per E-Mail benachrichtigt.
        ',
        'thankYouFooter' => 'Über den Wettbewerb wird keine Korrespondenz geführt.',
        'language' => 'de'
    );