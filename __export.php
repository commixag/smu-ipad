<?php
    require_once(__DIR__ . '/database.php');

    header("Cache-Control: no-cache, must-revalidate");
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/ms-excel');
    header('Content-Disposition: attachment; filename=participants_smu.xls');

    $db = new Database();
    $participants = $db->findAllParticipants();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>
        <table border="1" cellspacing="5">
            <thead>
                <tr>
                    <th>Vorname</th>
                    <th>Nachname</th>
                    <th>E-Mail</th>
                    <th>Frage</th>
                    <th>Antwort</th>
                    <th>Korrekt</th>
                    <th>Zeit</th>
                    <th>User Agent</th>
                    <th>Sprache</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($participants as $participant): ?>
                    <?php $date = new DateTime($participant['participation_time']); ?>
                    <tr>
                        <td valign="top" style="mso-number-format:\@;"><?php echo htmlentities($participant['first_name'], ENT_COMPAT, 'UTF-8'); ?></td>
                        <td valign="top" style="mso-number-format:\@;"><?php echo htmlentities($participant['last_name'], ENT_COMPAT, 'UTF-8'); ?></td>
                        <td valign="top" style="mso-number-format:\@;"><?php echo htmlentities($participant['email'], ENT_COMPAT, 'UTF-8'); ?></td>
                        <td valign="top" style="mso-number-format:\@;"><?php echo htmlentities($participant['question'], ENT_COMPAT, 'UTF-8'); ?></td>
                        <td valign="top" style="mso-number-format:\@;"><?php echo htmlentities($participant['answer'], ENT_COMPAT, 'UTF-8'); ?></td>
                        <td valign="top" style="mso-number-format:\@;"><?php echo $participant['answer_correct'] ? 'Ja' : 'Nein'; ?></td>
                        <td valign="top" style="mso-number-format:\@;"><?php echo $date->format('d.m.Y H:i:s'); ?></td>
                        <td valign="top" style="mso-number-format:\@;"><?php echo htmlentities($participant['user_agent'], ENT_COMPAT, 'UTF-8'); ?></td>
                        <td valign="top" style="mso-number-format:\@;"><?php echo htmlentities($participant['language'], ENT_COMPAT, 'UTF-8'); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </body>
</html>