<?php
    require_once(__DIR__ . '/language/de.php');

    $pageGerman = 'landtechnik-de';
    $pageFrench = 'technique-agricole';
    $style = 'agriculture';

    include(__DIR__ . '/partials/languageSwitch.php');