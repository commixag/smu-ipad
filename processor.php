<?php

require_once(__DIR__ . '/database.php');

class Processor {
    /**
     * @var array
     */
    private static $answers = array(
        'Landmaschinenmechaniker' => 'b,c,d',
        'Baumaschinenmechaniker' => 'c,d,e',
        'Motorgerätemechaniker' => 'a,c,e',
        'Metallbauer' => 'a,c,e',
        'Metallbaukonstrukteur' => 'b,d,e'
    );

    /**
     * @var Database
     */
    private $db;

    public function __construct() {
        $this->db = new Database();
    }

    private function checkExistence($email) {
        $this->sendJsonResponse(array(
            'exists' => $this->db->participantExists($email)
        ));
    }

    private function participate(array $values) {
        $this->db->addParticipant(
            $values['email'],
            $values['firstName'],
            $values['lastName'],
            $values['question'],
            $values['answer'],
            (self::$answers[$values['question']] == $values['answer']) + 0,
            $_SERVER['HTTP_USER_AGENT'],
            $values['language']
        );

        $this->sendJsonResponse(array());
    }

    public function process() {
        try {
            if($_REQUEST['action'] == 'checkExistence') {
                $this->checkExistence($_REQUEST['email']);
            } else if($_REQUEST['action'] == 'participate') {
                $this->participate($_REQUEST);
            } else {
                $this->sendJsonResponse(array(
                    'success' => false,
                    'message' => 'unknown action ' . $_REQUEST['action']
                ));
            }
        } catch(Exception $e) {
            $this->sendJsonResponse(array('success' => false, 'message' => $e->getMessage()));
        }
    }

    private function sendJsonResponse(array $response) {
        if(!array_key_exists('success', $response)) {
            $response['success'] = true;
        }

        if(!$response['success']) {
            header('Status: 400 Bad Request');
        }
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');

        echo json_encode($response);
    }
}

$processor = new Processor();
$processor->process();