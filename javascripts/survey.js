var question, //the current question
    currentPage, //the currently shown element
    selection, //the currently selected question
    currentParticipant, //the data of the current participant
    pageStack = [], //page navigation stack
    restartTimeout, //this timeout gets set at the end if the user does not click the back button to get back to the start screen
    spinner, //the load indicator
    remoteUrl = 'processor.php', //data endpoint
    participants = [], //offline storage
    resetTimeoutHandle,
    defaultTimeout = 30000,
    errorTimeoutHandle;

function clearForm() {
    $('#firstName').val('');
    $('#lastName').val('');
    $('#email').val('');
    $('.error').hide();
}

function onBackButtonClick(event) {
    if(arguments.length > 0) arguments[0].preventDefault();

    if(restartTimeout) clearTimeout(restartTimeout);

    if(pageStack.length > 0) {
        currentPage.addClass('right');
        currentPage = pageStack.pop().removeClass('left');
    }

    setResetTimeout(defaultTimeout);
}

function onNextButtonClick() { //nex button only available when answering questions
    if(arguments.length > 0) arguments[0].preventDefault();

    if(currentPage.attr('id') == 'form') {
        processRegistrationForm();
        setResetTimeout(defaultTimeout);
    } else if(currentPage.attr('id') == 'intro') {
        pageStack.push(currentPage.addClass('left'));
        currentPage = question = $('#' + currentPage.data('target')).removeClass('right');
        currentParticipant.question = question.data('question');
        setResetTimeout(defaultTimeout * 2);
    } else {
        if(currentPage.find(':checked').length > 0) {
            saveParticipation();
        } else {
            showError(language.noAnswers);
        }

        setResetTimeout(defaultTimeout);
    }
}

function onAjaxError() {
    reset(language.networkError);
}

function onExistenceChecked(data) {
    showLoading(false);

    if(data.exists) {
        reset(language.alreadyParticipated);
    } else {
        pageStack.push(currentPage.addClass('left'));
        currentPage = $('#intro').removeClass('right');
        question = $('[data-question="' + currentPage.data('question') + '"]');
        document.activeElement.blur(); //hides the keyboard
    }
}

function onParticipantSaved() {
    showLoading(false);
    pageStack.push(currentPage.addClass('left'));
    currentPage = $('#thankYou').removeClass('right');
    setResetTimeout(5000);
}

function onTileClick() {
    var target = $(this);
    selection = target.data('target');

    if(target.data('type') == 'question') {
        clearForm();

        pageStack.push(currentPage);
        currentPage.addClass('left');

        currentPage = $('#form').removeClass('right');
        $('#intro').data('target', selection);
    } else if(target.data('type') == 'page') {
        if($('#' + selection).length > 0) {
            pageStack.push(currentPage);
            currentPage.addClass('left');

            currentPage = $('#' + selection)
                .removeClass('right');
        }
    } else if(target.data('type') == 'remote') {
        location.href = target.data('target');
    }

    setResetTimeout(defaultTimeout);
}

function post(data, callback) {
    $.post(remoteUrl, data, function(response) {
        if(response.success) {
            callback.call(this, response);
        } else {
            onAjaxError();
        }
    }, 'json');
}

function processRegistrationForm() {
    if(validateTextField('firstName') && validateTextField('lastName') && validateEmailField('email')) {
        currentParticipant = {
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            email: $('#email').val()
        };

        if(navigator.onLine || !navigator.hasOwnProperty('onLine')) {
            showLoading(true);
            post({action: 'checkExistence', email: currentParticipant.email}, onExistenceChecked);
        } else {
            onExistenceChecked({exists: false});
        }
    }
}

function processQueue() {
    if(participants.length > 0 && navigator.onLine) {
        for(var i = 0; i < participants.length; i++) {
            post(participants[i]);
        }

        participants = [];
        localStorage.setItem('queue', JSON.stringify(participants));
    }
}

function reset() {
    if(arguments.length > 0) {
        location.href = resetURI + '?message=' + encodeURI(arguments[0]);
    } else {
        location.href = resetURI;
    }
}

function saveParticipation() {
    currentParticipant.answer = question.find(':checked').pluck('value').join(',');
    currentParticipant.action = 'participate';
    currentParticipant.language = language.language;

    if(navigator.onLine || !navigator.hasOwnProperty('onLine')) {
        showLoading(true);
        post(currentParticipant, onParticipantSaved);
    } else {
        participants.push(currentParticipant);
        localStorage.setItem('queue', JSON.stringify(participants));
        onParticipantSaved();
    }
}

function showError(message) {
    setTimeout(function() {
        $('#mainError').html(message).fadeIn();
    }, 0);

    if(errorTimeoutHandle != null) clearTimeout(errorTimeoutHandle);
    errorTimeoutHandle = setTimeout(function() {
        $('#mainError').fadeOut();
    }, 5000);
}

function showLoading(show) {
    if(show) {
        spinner.spin($('#loadIndicator').show().get(0));
    } else {
        spinner.stop();
        $('#loadIndicator').hide();
    }
}

function setResetTimeout(time) {
    if(resetTimeoutHandle) clearTimeout(resetTimeoutHandle);
    resetTimeoutHandle = setTimeout(reset, time);
}

function validateEmailField(id) {
    var field = $('#' + id);
    if(!field.val().match(/[a-z0-9-_.]+@[a-z0-9-_.]+\.[a-z]{2,}/i)) {
        $('#' + field.data('message')).show();

        return false;
    }

    $('#' + field.data('message')).hide();

    return true;
}

function validateTextField(id) {
    var field = $('#' + id);
    if(field.val().length < 2) {
        $('#' + field.data('message')).show();

        return false;
    }

    $('#' + field.data('message')).hide();

    return true;
}

$(function() {
    new FastClick(document.body);

    $('.tile').on('click', onTileClick);

    $('#form').submit(onNextButtonClick);

    $('input')
        .on('focus blur keypress', function() { setResetTimeout(defaultTimeout) });

    $(document)
        .on('ajaxError', onAjaxError)
        .on('click', '.next', onNextButtonClick)
        .on('click', '.back', onBackButtonClick)
        .on('touchend mouseup', function() { setResetTimeout(defaultTimeout) });

    currentPage = $('#start');

    participants = JSON.parse(localStorage.getItem('queue')) || [];

    spinner = new Spinner({
        lines: 15, // The number of lines to draw
        length: 24, // The length of each line
        width: 5, // The line thickness
        radius: 31, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        color: '#fff', // #rgb or #rrggbb
        speed: 1, // Rounds per second
        trail: 38, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: true, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 20, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px
    });

    setInterval(processQueue, 5000);
    setResetTimeout(defaultTimeout);

    if(typeof errorMessage != "undefined") {
        showError(errorMessage);
    }
});