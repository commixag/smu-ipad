<?php
    require_once(__DIR__ . '/language/de.php');

    $pageGerman = 'metallbau-de';
    $pageFrench = 'construction-metallique';
    $style = 'metal';

    include(__DIR__ . '/partials/languageSwitch.php');