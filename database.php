<?php

class Database {
    /**
     * @var PDO
     */
    private $db;

    public function __construct() {
        $this->initializeDatabase();
    }

    public function addParticipant($email, $firstName, $lastName, $question, $answer, $answerCorrect, $userAgent, $language) {
        $statement = $this->db->prepare("
            INSERT INTO participants (email, first_name, last_name, question, answer, answer_correct, user_agent, language) VALUES
            (:email, :firstName, :lastName, :question, :answer, :answerCorrect, :userAgent, :language)
        ");

        $statement->execute(array(
            ':email' => $email,
            ':firstName' => $firstName,
            ':lastName' => $lastName,
            ':question' => $question,
            ':answer' => $answer,
            ':answerCorrect' => $answerCorrect,
            ':userAgent' => $userAgent,
            ':language' => $language
        ));
    }

    public function findAllParticipants() {
        $statement = $this->db->query('SELECT * FROM participants ORDER BY first_name, last_name');

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    private function initializeDatabase() {
        $dbFile = __DIR__ . '/participants.db';

        if(!file_exists($dbFile)) {
            $this->db = new PDO('sqlite:' . $dbFile);
            $this->db->query("
                CREATE TABLE participants (
                  email VARCHAR(120) PRIMARY KEY ASC,
                  first_name VARCHAR(120) NOT NULL,
                  last_name VARCHAR(120) NOT NULL,
                  question VARCHAR(120) NOT NULL,
                  answer VARCHAR(120) NOT NULL,
                  answer_correct TINYINT NOT NULL,
                  participation_time DATETIME NOT NULL DEFAULT (datetime('now','localtime')),
                  user_agent TEXT NOT NULL,
                  language VARCHAR(2) NOT NULL
                )
            ");
        } else {
            $this->db = new PDO('sqlite:' . $dbFile);
        }
    }

    public function participantExists($email) {
        $statement = $this->db->prepare('SELECT COUNT(email) FROM participants WHERE email LIKE :email');
        $statement->execute(array(':email' => $email));

        return ($statement->fetchColumn(0) > 0);
    }
}
