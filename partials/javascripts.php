<script>
    var language = {
        alreadyParticipated: "<?php echo $language['alreadyParticipated']; ?>",
        networkError: "<?php echo $language['networkError']; ?>",
        noAnswers: "<?php echo $language['noAnswers']; ?>",
        language: "<?php echo $language['language']; ?>"
    };
</script>
<script src="javascripts/fastclick.min.js"></script>
<script src="javascripts/zepto.min.js"></script>
<script src="javascripts/spin.min.js"></script>
<script src="javascripts/survey.js"></script>