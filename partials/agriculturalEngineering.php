<!DOCTYPE html>
<html class="agriculture" manifest="manifest.mf">
<head>
    <?php include(__DIR__ . '/head.php'); ?>
    <link rel="stylesheet" type="text/css" href="stylesheets/agriculture.css">
</head>
<body>
    <div class="error main-error" id="mainError" style="display: none;"></div>
    <div class="load-indicator" id="loadIndicator" style="display: none;"></div>
    <div class="main">
        <img src="images/logo.png" class="logo">

        <?php include(__DIR__ . '/start.php'); ?>

        <div class="page right" id="overview">
            <div class="page-inner">
                <div class="tile" data-type="question" data-target="mechanicsQuestion1">
                    <div class="tile-heading">
                        <?php echo $language['playWin']; ?>
                    </div>

                    <div class="tile-caption">
                        <?php echo $language['mq1']; ?>
                    </div>
                </div>

                <div class="tile" data-type="question" data-target="mechanicsQuestion2">
                    <div class="tile-heading">
                        <?php echo $language['playWin']; ?>
                    </div>

                    <div class="tile-caption">
                        <?php echo $language['mq2']; ?>
                    </div>
                </div>

                <div class="tile" data-type="question" data-target="mechanicsQuestion3">
                    <div class="tile-heading">
                        <?php echo $language['playWin']; ?>
                    </div>

                    <div class="tile-caption">
                        <?php echo $language['mq3']; ?>
                    </div>
                </div>

                <div class="nav">
                    <div class="back"></div>
                </div>
            </div>
        </div>

        <div class="page question right" id="mechanicsQuestion1" data-question="Landmaschinenmechaniker" data-answer="b,c,d">
            <div class="page-inner">
                <p><?php echo $language['mq1Head']; ?></p>

                <p>
                    <input type="checkbox" value="a" id="mq1a">
                    <label for="mq1a"><?php echo $language['mq1a']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="b" id="mq1b">
                    <label for="mq1b"><?php echo $language['mq1b']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="c" id="mq1c">
                    <label for="mq1c"><?php echo $language['mq1c']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="d" id="mq1d">
                    <label for="mq1d"><?php echo $language['mq1d']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="e" id="mq1e">
                    <label for="mq1e"><?php echo $language['mq1e']; ?></label>
                </p>

                <?php include(__DIR__ . '/nav.php'); ?>
            </div>
        </div>

        <div class="page question right" id="mechanicsQuestion2" data-question="Baumaschinenmechaniker" data-answer="c,d,e">
            <div class="page-inner">
                <p><?php echo $language['mq2Head']; ?></p>

                <p>
                    <input type="checkbox" value="a" id="mq2a">
                    <label for="mq2a"><?php echo $language['mq2a']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="b" id="mq2b">
                    <label for="mq2b"><?php echo $language['mq2b']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="c" id="mq2c">
                    <label for="mq2c"><?php echo $language['mq2c']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="d" id="mq2d">
                    <label for="mq2d"><?php echo $language['mq2d']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="e" id="mq2e">
                    <label for="mq2e"><?php echo $language['mq2e']; ?></label>
                </p>

                <?php include(__DIR__ . '/nav.php'); ?>
            </div>
        </div>

        <div class="page question right" id="mechanicsQuestion3" data-question="Motorgerätemechaniker" data-answer="a,c,e">
            <div class="page-inner">
                <p><?php echo $language['mq3Head']; ?></p>

                <p>
                    <input type="checkbox" value="a" id="mq3a">
                    <label for="mq3a"><?php echo $language['mq3a']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="b" id="mq3b">
                    <label for="mq3b"><?php echo $language['mq3b']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="c" id="mq3c">
                    <label for="mq3c"><?php echo $language['mq3c']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="d" id="mq3d">
                    <label for="mq3d"><?php echo $language['mq3d']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="e" id="mq3e">
                    <label for="mq3e"><?php echo $language['mq3e']; ?></label>
                </p>

                <?php include(__DIR__ . '/nav.php'); ?>
            </div>
        </div>

        <?php include(__DIR__ . '/form.php'); ?>
        <?php include(__DIR__ . '/intro.php'); ?>
        <?php include(__DIR__ . '/thankYou.php'); ?>
    </div>

    <script>
        var resetURI = 'landtechnik-de.php';
    </script>
    <?php include(__DIR__ . '/javascripts.php'); ?>
</body>
</html>