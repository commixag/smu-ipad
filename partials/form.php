<form action="index.html" class="page right" method="post" id="form">
    <h1><?php echo $language['formHead']; ?></h1>

    <p>
        <input type="text" name="firstName" id="firstName" required data-message="firstNameError" placeholder="<?php echo $language['firstName']; ?>">
    </p>
    <p id="firstNameError" class="error" style="display: none;">
        <?php echo $language['firstNameError']; ?>
    </p>
    <p>
        <input type="text" name="lastName" id="lastName" required data-message="lastNameError" placeholder="<?php echo $language['lastName']; ?>">
    </p>
    <p id="lastNameError" class="error" style="display: none;">
        <?php echo $language['lastNameError']; ?>
    </p>
    <p>
        <input type="email" name="email" id="email" required data-message="emailError" placeholder="<?php echo $language['email']; ?>">
    </p>
    <p id="emailError" class="error" style="display: none;">
        <?php echo $language['emailError']; ?>
    </p>
    <p style="visibility: hidden;">
        <input type="submit" value="Eingabe bestätigen">
    </p>

    <?php include(__DIR__ . '/nav.php'); ?>
</form>