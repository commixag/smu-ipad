<div class="page" id="start">
    <p>
        <?php echo $language['startText']; ?>
    </p>

    <div class="tile" data-type="page" data-target="overview">
        <div class="tile-caption">
            <?php echo $language['startCaption']; ?>
        </div>
    </div>
</div>