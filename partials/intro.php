<div class="page right" id="intro">
    <h1><?php echo $language['introHead']; ?></h1>
    <p>
        <?php echo $language['introText']; ?>
    </p>
    <div class="nav">
        <div class="back"></div>
        <div class="next"></div>
    </div>
</div>

