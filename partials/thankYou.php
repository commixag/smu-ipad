<div class="page right" id="thankYou">
    <h1><?php echo $language['thankYouHead']; ?></h1>
    <p>
        <?php echo $language['thankYouText']; ?>
    </p>
    <p>
        <small><?php echo $language['thankYouFooter']; ?></small>
    </p>
</div>