<!DOCTYPE html>
<html class="metal" manifest="manifest.mf">
<head>
    <?php include(__DIR__ . '/head.php'); ?>
    <link rel="stylesheet" type="text/css" href="stylesheets/<?php echo $style; ?>.css">
</head>
<body>
    <div class="error main-error" id="mainError" style="display: none;"></div>
    <div class="load-indicator" id="loadIndicator" style="display: none;"></div>

    <div class="main">
        <img src="images/logo.png" class="logo">

        <div class="page" id="languageSwitch">
            <div class="page-inner">
                <div class="tile" data-type="remote" data-target="<?php echo $pageGerman; ?>.php">
                    <div class="tile-heading">
                        Deutsch
                    </div>
                </div>

                <div class="tile" data-type="remote" data-target="<?php echo $pageFrench; ?>.php">
                    <div class="tile-heading">
                        Fran&ccedil;ais
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if(array_key_exists('message', $_GET)): ?>
        <script>
            var errorMessage = decodeURI('<?php echo $_GET['message']; ?>');
        </script>
    <?php endif; ?>
    <script>var resetURI = window.location.href;</script>
    <?php include(__DIR__ . '/javascripts.php'); ?>
</body>
</html>