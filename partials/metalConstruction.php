<!DOCTYPE html>
<html class="metal" manifest="manifest.mf">
<head>
    <?php include(__DIR__ . '/head.php'); ?>
    <link rel="stylesheet" type="text/css" href="stylesheets/metal.css">
</head>
<body>
    <div class="error main-error" id="mainError" style="display: none;"></div>
    <div class="load-indicator" id="loadIndicator" style="display: none;"></div>

    <div class="main">
        <img src="images/logo.png" class="logo">

        <?php include(__DIR__ . '/start.php'); ?>

        <div class="page right" id="overview">
            <div class="page-inner">
                <div class="tile" data-type="question" data-target="metalQuestion1">
                    <div class="tile-heading">
                        <?php echo $language['playWin']; ?>
                    </div>

                    <div class="tile-caption">
                        <?php echo $language['mtq1']; ?>
                    </div>
                </div>

                <div class="tile" data-type="question" data-target="metalQuestion2">
                    <div class="tile-heading">
                        <?php echo $language['playWin']; ?>
                    </div>

                    <div class="tile-caption">
                        <?php echo $language['mtq2']; ?>
                    </div>
                </div>

                <div class="nav">
                    <div class="back"></div>
                </div>
            </div>
        </div>

        <div class="page question right" id="metalQuestion1" data-question="Metallbauer" data-answer="a,c,e">
            <div class="page-inner">
                <p><?php echo $language['mtq1Head']; ?></p>

                <p>
                    <input type="checkbox" value="a" id="mtq1a">
                    <label for="mtq1a"><?php echo $language['mtq1a']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="b" id="mtq1b">
                    <label for="mtq1b"><?php echo $language['mtq1b']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="c" id="mtq1c">
                    <label for="mtq1c"><?php echo $language['mtq1c']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="d" id="mtq1d">
                    <label for="mtq1d"><?php echo $language['mtq1d']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="e" id="mtq1e">
                    <label for="mtq1e"><?php echo $language['mtq1e']; ?></label>
                </p>

                <?php include(__DIR__ . '/nav.php'); ?>
            </div>
        </div>

        <div class="page question right" id="metalQuestion2" data-question="Metallbaukonstrukteur" data-answer="b,d,e">
            <div class="page-inner">
                <p><?php echo $language['mtq2Head']; ?></p>

                <p>
                    <input type="checkbox" value="a" id="mtq2a">
                    <label for="mtq2a"><?php echo $language['mtq2a']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="b" id="mtq2b">
                    <label for="mtq2b"><?php echo $language['mtq2b']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="c" id="mtq2c">
                    <label for="mtq2c"><?php echo $language['mtq2c']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="d" id="mtq2d">
                    <label for="mtq2d"><?php echo $language['mtq2d']; ?></label>
                </p>

                <p>
                    <input type="checkbox" value="e" id="mtq2e">
                    <label for="mtq2e"><?php echo $language['mtq2e']; ?></label>
                </p>

                <?php include(__DIR__ . '/nav.php'); ?>
            </div>
        </div>

        <?php include(__DIR__ . '/form.php'); ?>
        <?php include(__DIR__ . '/intro.php'); ?>
        <?php include(__DIR__ . '/thankYou.php'); ?>
    </div>

    <script>
        var resetURI = 'metallbau.php';
    </script>
    <?php include(__DIR__ . '/javascripts.php'); ?>
</body>
</html>